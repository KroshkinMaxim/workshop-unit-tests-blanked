DROP DATABASE IF EXISTS customer;

CREATE DATABASE customer
    WITH
    OWNER = workshop
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

DROP DATABASE IF EXISTS fraud;

CREATE DATABASE fraud
    WITH
    OWNER = workshop
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;